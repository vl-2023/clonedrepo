import Article from "../../components/article/article"
import Layout from "../../components/layout/layout"
import Paragraph from "../../components/paragraph/paragraph"
import { Heading } from "../../components/agenda"
import Texts from "../../components/textFormat/text/text"
import { loremMultiParagaph } from "../../importStatic/mockData/lorem"
import Refrences from "../../components/textFormat/references/references"
import ReferenceList from "../../referenceList/referenceList"


const descriptionPage = () => {
    return(
        <>
         <Layout
            isWithAgenda 
            agendaTitle="Description"
            isWithProgressBar
            agendaHeight="h-[70vh]"
              >
            <Article>
                <Paragraph>

                    <Texts.Header>
                        <Heading  as = "h1" id="sumup">
                        Summary
                        </Heading>
                    </Texts.Header>
                    <Paragraph>
                    We aim to create a biological system that would overcome the limitations of chemical modifications
                    and expand potential bacterial cellulose applications by 
                    creating an easily modifiable bacterial cellulose-chitosan polymer.
                    </Paragraph>

                    <Texts.Header>
                        <Heading  as = "h1" id="overview">
                        Overview
                        </Heading>
                    </Texts.Header>
                    <Paragraph>
                    Biopolymers are one of the most prevalent macromolecules in nature and play an important role in human life. 
                    These materials offer a wide range of applications in solving critical problems associated with medicine and sustainability due to their biocompatibility and biodegradability. 
                    As polymers are composed of repeating molecules, the variety of combinations is virtually endless <a href="#first">[1]</a>.
                    <br/>
                    <br/>
                    Our team has taken an interest in one of the most essential biopolymers: cellulose. 
                    Cellulose is a carbohydrate that occurs naturally in plant cell walls, making it one of the most common materials on the Earth’s surface <a href="#second">[2]</a>. 
                    Cellulose is also found to be produced by bacteria such as Acetobacter, with a slightly different structure and characteristics than its plant-originated counterpart <a href="#third">[3]</a>. 
                    Bacterial cellulose has a high crystalline structure and a high tensile strength which is not observed in plant cellulose, due to the absence of heterologous polymers, such as hemicellulose, pectin, and lignin <a href="#4th">[4]</a>. 
                    Bacterial cellulose (BC) also has hydrophilic interactions and a large surface area, enabling high water retention. 
                    Furthermore, this polymer is generally nontoxic, establishing it as a valuable base material for biomedical applications<a href="#5th">[5]</a>.
                    </Paragraph>

                    <Texts.Header>
                        <Heading  as = "h1" id="revalance">
                        Project revelance
                        </Heading>
                    </Texts.Header>
                    <Paragraph>
                    The unique properties of BC make it a desirable polymer - the estimated projected compound annual growth rate for this type of product has been around 10.2% in recent years <a href="#6th">[6]</a>. 
                    However, further market demand for this exopolysaccharide is limited due to many drawbacks that curb its application on the industrial scale. 
                    Inadequate understanding of the bacteria and the factors affecting large-scale BC production and the presence of strong inter/intramolecular hydrogen bonds in BC is a limiting factor in its utilization and functionalization. <a href="#7th">[7]</a>
                    </Paragraph>

                    <Texts.Header>
                        <Heading  as = "h1" id="problem">
                        The problem
                        </Heading>
                    </Texts.Header>
                    <Paragraph>
                    For decades, research on bacterial cellulose concentrated on optimizing polymer production through regulating environmental conditions. 
                    This focus on production optimization rather than functionalization hindered the full potential of bacterial cellulose; therefore, there is an increasing interest in developing BC-based hybrid materials [4]. 
                    For example, bacterial cellulose can be combined with chitosan <a href="#7th">[7]</a>. 
                    Chitosan is acquired by chemical or enzymatic deacetylation from chitin, a polymer found in arthropods’ exoskeleton and fungi’s cell walls <a href="#8th">[8]</a>. 
                    This copolymer is considered superior to cellulose and chitosan separately because it combines high water holding capacity, hydrophilicity, structural and mechanical qualities of cellulose with electron donors and the antibacterial activity of chitosan [9]. 
                    The most common method of obtaining bacterial cellulose-chitosan polymer is by physically mixing these two polymers. 
                    However, these methods give rise to materials with uneven distribution of cellulose and chitosan throughout the polymer. 
                    On top of that, chitosan slowly diffuses out of bacterial cellulose, changing the biocomposites' properties over time<a href="#8th">[8]</a>.
                    </Paragraph>

                    <Texts.Header>
                        <Heading  as = "h1" id="solution">
                        The solution
                        </Heading>
                    </Texts.Header>
                    <Paragraph>
                    Exullose aims to solve some of the current bacterial cellulose limitations, including complex functionalization <a href="#4th">[4]</a>, poor in vivo biodegradability <a href="#10th">[10]</a> and the absence of antibacterial activity <a href="#11th">[11]</a>.
                    <br/>
                    <br/>
                    We will achieve this by creating a novel process for producing bacterial cellulose-chitosan copolymer. 
                    We were inspired by the works of Yadav (2010) and Teh (2019), which introduced in situ dynamic biosynthesis of bacterial cellulose-chitin copolymer. 
                    We will enhance N-acetylglucosamine incorporation into cellulose fibrils by introducing additional GlcNac metabolism enzymes: GlcNac kinase (NAG5), 
                    phosphoacetylglucosamine mutase (AGM1) and UDP-GlcNac diphosphorylase (UAP1) <a href="#10th">[10]</a>,<a href="#12th">[12]</a>. 
                    After obtaining cellulose-chitin polymer, we plan to produce cellulose-chitosan copolymer by deacetylating N-acetylglucosamine residues with broad substrate range deacetylases. 
                    Amino groups provided by chitosan can be modified chemically or enzymatically <a href="#13th">[13]</a>, providing enhanced material properties compared to cellulose alone.
                    </Paragraph>

                    <Texts.Header>
                        <Heading  as = "h1" id="implementations">
                        Future implementations
                        </Heading>
                    </Texts.Header>
                    <Paragraph>
                    Amino groups in bacterial cellulose-chitosan polymer offer a versatile platform for a vast spectrum of modifications. 
                    Examples mentioned in the literature include:
                    <ul>
                        <li>The addition of amino acids to this polymer accelerates wound healing <a href="#14th">[14]</a>;</li>
                        <li>Bacterial cellulose-chitosan polymer and other bioactive molecule conjugates create specialized and advanced drug delivery systems <a href="#15th">[15]</a>;</li>
                        <li>Additionally, the bacterial cellulose-chitosan biopolymer can be used for sustainable food packaging <a href="#16th">[16]</a>.</li>
                    </ul>
                    We aim for Exullose to enable the aforementioned applications as well as other potential applications.
                    </Paragraph>

                    <Texts.Header>
                        <Heading  as = "h1" id="refrences">
                        Refrences
                        </Heading>
                    </Texts.Header>
                    <Paragraph>    
                        <ReferenceList>
                        <ol>
                            <li id="first">Baranwal, J. et al. (2022) <a href="http://doi.org/10.3390/polym14050983">‘Biopolymer: A sustainable material for food and medical applications’</a>, Polymers, 14(5), p. 983. doi:10.3390/polym14050983.</li>
                            <li id = "second">Klemm, D. et al. (2005) <a href=" https://doi.org/10.1002/anie.200460587">‘Cellulose: Fascinating biopolymer and sustainable raw material’</a>, Angewandte Chemie International Edition, 44(22), pp. 3358–3393. doi:10.1002/anie.200460587.</li>
                            <li id='third' >Lahiri, D. et al. (2021) <a href = 'http://doi.org/10.3390/ijms222312984' > Bacterial cellulose: Production, characterization, and application as antimicrobial agent</a>, International Journal of Molecular Sciences, 22(23), p. 12984. doi:10.3390/ijms222312984.</li>
                            <li id='4th' >Buldum, G. and Mantalaris, A. (2021) <a href = 'http://doi.org/10.3390/ijms22137192' > Systematic understanding of recent developments in bacterial cellulose biosynthesis at genetic, bioprocess and product levels</a>, International Journal of Molecular Sciences, 22(13), p. 7192. doi:10.3390/ijms22137192.</li>
                            <li id='5th' >Manoukian, O.S. et al. (2019) <a href = 'http://doi.org/10.1016/b978-0-12-801238-3.64098-9' > Biomaterials for tissue engineering and Regenerative Medicine</a>, Encyclopedia of Biomedical Engineering, pp. 462–482. doi:10.1016/b978-0-12-801238-3.64098-9.</li>
                            <li id= '6th'> Publishing, B. (n.d.). Global Microbial Products Market: Technologies & Applications. BCC Research LLC. <a href="https://www.bccresearch.com/market-research/biotechnology/microbial-products-technologies-applications-and-global-markets-report.html">https://www.bccresearch.com/market-research/biotechnology/microbial-products-technologies-applications-and-global-markets-report.html</a></li>
                            <li id='7th' > Zhang, P. et al. (2016) <a href = 'http://doi.org/10.3389/fmicb.2016.00260' > Using in situ dynamic cultures to rapidly biofabricate fabric-reinforced composites of chitosan/bacterial nanocellulose for antibacterial wound dressings</a>, Frontiers in Microbiology, 7. doi:10.3389/fmicb.2016.00260.</li>
                            <li id= '8th'>Khalil, H.P.S.A., Saurabh, C.K., AS, A., Nurul Fazita, M.R., Syakir, M.I., Davoudpour, Y., Rafatullah, M. and Abdullah, C.K., M. Haafiz, MK, and Dungani, R.(2016)."<a href="http://doi.org/10.1016/j.carbpol.2016.05.028">A review on chitosan-cellulose blends and nanocellulose reinforced chitosan biocomposites: Properties and their applications,"</a>. Carbohydr. Polym, 150, pp.216-226. doi:10.1016/j.carbpol.2016.05.028</li>
                            <li id='9th' >Strnad, S. and Zemljič, L.F., 2023. <a href = 'http://doi.org/10.3390/polym15020425' > Cellulose–Chitosan Functional Biocomposites</a>. Polymers, 15(2), p.425. doi:10.3390/polym15020425</li>
                            <li id='10th' >Yadav, V. et al. (2010) <a href = 'http://doi.org/10.1128/aem.00698-10' > Novel in vivo degradable cellulose-chitin copolymer from metabolically engineered gluconacetobacter xylinus</a>, Applied and Environmental Microbiology, 76(18), pp. 6257–6265. doi:10.1128/aem.00698-10.</li>
                            <li id='11th' >Wei, B., Yang, G. and Hong, F. (2011) <a href = 'http://doi.org/10.1016/j.carbpol.2010.12.017' > Preparation and evaluation of a kind of bacterial cellulose dry films with antibacterial properties</a>, Carbohydrate Polymers, 84(1), pp. 533–538. doi:10.1016/j.carbpol.2010.12.017.</li>
                            <li id='12th' >Teh, M.Y. et al. (2019) <a href = 'http://doi.org/10.1021/acssynbio.8b00168' > An expanded synthetic biology toolkit for gene expression control in acetobacteraceae</a>, ACS Synthetic Biology, 8(4), pp. 708–723. doi:10.1021/acssynbio.8b00168.</li>
                            <li id='13th' >Mittal, H., Ray, S.S., Kaith, B.S., Bhatia, J.K., Sharma, J. and Alhassan, S.M., 2018. <a href = 'https://doi.org/10.1016/j.eurpolymj.2018.10.013' > Recent progress in the structural modification of chitosan for applications in diversified biomedical fields</a>. European Polymer Journal, 109, pp.402-434. doi:10.1016/j.eurpolymj.2018.10.013</li>
                            <li id='14th' >Torkaman, S. et al. (2021) <a href = 'http://doi.org/10.1016/j.carbpol.2021.117675' > Modification of chitosan using amino acids for wound healing purposes: A Review</a>, Carbohydrate Polymers, 258, p. 117675. doi:10.1016/j.carbpol.2021.117675.</li>
                            <li id='15th' >Basu, A. et al. (2015) <a href = 'http://doi.org/10.1021/acs.bioconjchem.5b00242' > Polysaccharide-based conjugates for biomedical applications</a>, Bioconjugate Chemistry, 26(8), pp. 1396–1412. doi:10.1021/acs.bioconjchem.5b00242.</li>
                            <li id='16th' >Lima, R., Fernandes, C. and Pinto, M.M. (2022) <a href = 'http://doi.org/10.1002/chir.23477' > Molecular modifications, biological activities, and applications of chitosan and derivatives: A recent update</a>, Chirality, 34(9), pp. 1166–1190. doi:10.1002/chir.23477.</li>  
                        </ol> 
                        </ReferenceList>
                    </Paragraph>
                </Paragraph>
            </Article>
        </Layout>
        </>
    )
}
export default descriptionPage;