import Article from "../../components/article/article"
import Layout from "../../components/layout/layout"
import Paragraph from "../../components/paragraph/paragraph"
import { Heading } from "../../components/agenda"
import Texts from "../../components/textFormat/text/text"
import { loremMultiParagaph } from "../../importStatic/mockData/lorem"

const collaborationsPage = () => {
    return(
        <>
         <Layout
            isWithAgenda 
            agendaTitle="Communication"
            isWithProgressBar
            agendaHeight="h-[70vh]"
              >
            <Article>
                <Paragraph>
                    <Texts.Header>
                        <Heading  as = "h1" id="end">
                            The end
                        </Heading>
                    </Texts.Header>
                    <Paragraph>
                        {loremMultiParagaph}
                    </Paragraph>
                </Paragraph>
            </Article>
        </Layout>
        </>
    )
}
export default collaborationsPage