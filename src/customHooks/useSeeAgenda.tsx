import { useEffect, useState } from "react"
import getDocumentHeight from "../components/progressBar/getDocHeight"

const useSeeAgenda = (footerHeight: number) => {
  const [isShown, setIsShown] = useState(false)

  useEffect(() => {
    const updateScrollCompletion = () => {
      if (typeof window !== "undefined") {
        const scrollTop =
          window.scrollY ||
          (
            document.documentElement ||
            document.body.parentNode ||
            document.body
          ).scrollTop

        if (
          getDocumentHeight() - window.innerHeight * 0.8 - scrollTop <
            footerHeight * 1.1 ||
          scrollTop < window.innerHeight * 0.8
        ) {
          setIsShown(false)
        } else {
          setIsShown(true)
        }
      }
    }

    window.addEventListener("scroll", updateScrollCompletion)

    return () => {
      window.removeEventListener("scroll", updateScrollCompletion)
    }
  }, [])

  return isShown
}

export default useSeeAgenda

