import { useEffect, useState, RefObject } from 'react';

interface UseElementVisibilityProps {
  root?: HTMLElement | null;
  rootMargin?: string;
  threshold?: number | number[];
}

const useElementVisibility = (
  targetRef: RefObject<HTMLElement>,
  options: UseElementVisibilityProps = {}
): number => {
  const { root = null, rootMargin = '0px', threshold = 0 } = options;
  const [visibilityPercentage, setVisibilityPercentage] = useState(0);

  useEffect(() => {
    if (targetRef.current) {
      const observer = new IntersectionObserver(
        ([entry]) => {
          const { intersectionRatio } = entry;
          // Calculate the percentage of the element in the viewport
          //const percentage = Math.round(intersectionRatio * 100);
          const percentage = intersectionRatio * 100;
          setVisibilityPercentage(percentage);
        },
        {
          root,
          rootMargin,
          threshold,
        }
      );

      observer.observe(targetRef.current);

      return () => {
        if (targetRef.current) {
          observer.unobserve(targetRef.current);
        }
      };
    }
  }, [targetRef, root, rootMargin, threshold]);

  return visibilityPercentage;
};

export default useElementVisibility;
