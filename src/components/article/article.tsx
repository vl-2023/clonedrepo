import React from "react";
import "./article.css"; 
type Props = {
  children: React.ReactNode;
  style?: React.CSSProperties;
};

const Article = ({ children, style }: Props) => {
  return (
    <div className="article-container" style={style}>
      <article className="article-content">{children}</article>
    </div>
  );
};

export default Article;