import React from "react"
import useSeeAgenda from "../../customHooks/useSeeAgenda"
import useWindowDimensions from "../../customHooks/useWindowDimensions"
import getFooterHeight from "../../customHooks/getFooterHeight"
import { Agenda, AgendaContextProvider } from "../agenda"
import Footer from "../footer/footer"
//import MainHeader from "../MainHeader"
//import { MainHeaders } from "../MainHeader/types/mainHeaders"
import Navigation from "../navbar"
import ProgressBar from "../../components/progressBar/index/progressBar"
import MainHeader from "../header/index/headerMain"
import { MainHeaders } from "../header/headerTypes"


type Props = {
  agendaTitle?: string
  children: React.ReactNode
  //isWithBackToTop?: boolean
  isWithAgenda?: boolean
  isWithProgressBar?: boolean
  isWithBackground?: boolean
  //header: MainHeaders
  agendaHeight?: string
  //isTurtleSmart?: boolean
  isPageLong?: boolean
}

const Layout = ({
  agendaTitle,
  children,
  //isWithBackToTop = false,
  isWithAgenda = false,
  isWithProgressBar = false,
  isWithBackground = true,
  //header,
  agendaHeight,
  //isTurtleSmart = false,
  isPageLong,
}: Props) => {
  const { width } = useWindowDimensions()
  const footerHeight = getFooterHeight(width || 0)
  const isShown = useSeeAgenda(footerHeight)
  const renderChildren = () => (
    <AgendaContextProvider isWithAgenda={isWithAgenda}>
      <>{children}</>
    </AgendaContextProvider>
  )

  return (
    <>
      <Navigation />
      <MainHeader />
        {renderChildren()}
        {isWithAgenda && (
          <Agenda
            title={agendaTitle || "Agenda title"}
            isShown={isShown}
            mobileHeight={agendaHeight}
          />
        )}
        {isWithProgressBar && (
          <ProgressBar isShown={isShown} footerHeight={footerHeight} />
        )}
      <Footer />
    </>
  )
}

export default Layout
