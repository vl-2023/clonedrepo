import React from "react"
import { AgendaProps } from "../../types/agendaTypes"
import "./desktopAgenda.css"
import HeadingText from "./headingText"

export default function DesktopAgenda({
  activeId,
  isShown = true,
  headings,
  onScroll,
  title,
}: AgendaProps) {
  return (
    <nav
      className={` ${isShown ? "scrollbar": "inactive"}`}>
      <span className="font-jost-medium">
        {title}
      </span>
      <ul>
        {headings.map(heading => (
          <li
            key={heading.id}
            className="verticalDisplay"
            onClick={_e => onScroll(heading.id)}
            style={{
              marginLeft: `${heading.level - 1}em`,
            }}
          >
            <HeadingText>{heading.text}</HeadingText>
          </li>
        ))}
      </ul>
    </nav>
  )
}
