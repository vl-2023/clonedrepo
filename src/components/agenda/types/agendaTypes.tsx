export type HeadingType = { id: string; text: string; level: number }

export interface AgendaProps {
  activeId: string | undefined
  isShown?: boolean
  headings: HeadingType[]
  onScroll: (id: string) => void
  title: string
}

export interface MobileAgendaProps extends AgendaProps {
  listHeight?: string
}
