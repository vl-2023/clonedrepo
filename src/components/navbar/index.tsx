import React from "react"
import useWindowDimensions from "../../customHooks/useWindowDimensions";
import { CSSProperties } from "react";
import { smWidth } from "../../importStatic/sizeConsts"
import DesktopNavBar from "./components/desktopNavBar/desktopnavbar";
import MobileNavBar from "./components/mobileNavBar/mobilenavbar";
const NavBar = () => {
    const navBarStyle: CSSProperties = {
        position: "sticky",
        top: 0,
        zIndex: 30
      };      
    const {width} = useWindowDimensions()
    return(
        <nav style={navBarStyle}>
            {width && width > smWidth ? <DesktopNavBar/> : <MobileNavBar />}
        </nav>
    )
}
export default NavBar;