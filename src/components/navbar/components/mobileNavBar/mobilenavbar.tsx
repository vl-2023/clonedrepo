import React from "react"
import { NavLink } from "react-router-dom";

const MobileNavBar = () => (
    <NavLink
    to="/vilnius-lithuania/attributions"
    className={({ isActive, isPending }) =>
      isPending ? "pending" : isActive ? "active" : ""
    }
  >
    Attributions
  </NavLink>
)
export default MobileNavBar