import React from "react"
import List from "./Components/List"
import { listItems } from "./Components/types/ListItem"
import "./desktopnavbar.css"
import { logoexullose } from "../../../../importStatic/mockData/images"
import { useNavigate } from "react-router-dom"

const DesktopNavBar = () => {

    const navigate = useNavigate();
  

    return(
    <div className="desktopNavCointainer">
        <div className="logoSmallContainer" ><img className="logoSmall" onClick={()=>navigate("/vilnius-lithuania/")} src={logoexullose} alt="bbz" /></div>
        <div className="listContainer"><List listItems={listItems} /></div>
    </div>
    )

}
export default DesktopNavBar