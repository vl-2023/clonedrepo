import React from "react"
import Button from "../../../../button/button"

type Props = {
  children: React.ReactNode
  isOpen?: boolean
}

const ListItemContent = ({ children, }: Props) => (
  <div className="listItemContent">
      <Button onClick={()=> console.log("s")}>
        {children}
      </Button>
      <div className="listAllcontent">
      </div>
  </div>
)

export default ListItemContent
