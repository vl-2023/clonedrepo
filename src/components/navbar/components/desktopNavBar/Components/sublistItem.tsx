import React from "react";
import { PageRoute } from "./types/links";
import Button from "../../../../button/button";
import { useNavigate } from "react-router-dom";
import "./sublistItem.css"; 

type Props = {
  isFirst: boolean;
  isLast: boolean;
  isOpen: boolean;
  subItem: PageRoute;
};

const SubListItem = ({ isFirst, isLast, isOpen, subItem }: Props) => {
  const navigate = useNavigate();
  
  return (
    <li
      className={`sub-list-item ${isFirst ? "rounded-t" : ""} ${
        isLast ? "rounded-b" : ""
      } ${isOpen ? "opacity-100 cursor-pointer" : "opacity-0 cursor-default"}`}
      onClick={() => {
        if (isOpen) {
          navigate(`/vilnius-lithuania/${subItem.label}`);
        }
      }}
    >
      <Button onClick={() => console.log("sublistItem")}>
        {subItem.label}
      </Button>
    </li>
  );
};

export default SubListItem;
