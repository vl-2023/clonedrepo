import React from "react"
import { PageSections } from "./types/links"
import OutsideAlerter from "../../../../OutsideAlerter/OutsideAlerter"
import SubList from "./SubList"
import ListItemContent from "./ListItemContent"
import "./ListItem.css"

type Props = {
  children: React.ReactNode
  isLast: boolean
  section: PageSections
}

const ListItem = ({ children, isLast, section }: Props) => {
  const [isOpen, setIsOpen] = React.useState<boolean>(false)
  const handleClick = () => {
    setIsOpen(!isOpen)
  }

  const handleKeyDown = (e: React.KeyboardEvent<HTMLLIElement>) => {
    if (e.key === "Enter") {
      setIsOpen(!isOpen)
    }

    if (e.key === "Tab" && isOpen) {
      setIsOpen(false)
    }
  }

  return (
    <OutsideAlerter onClick={() => setIsOpen(false)}>
      <li
        className={`listItemss`}
        onClick={handleClick}
        tabIndex={0}
        onKeyDown={handleKeyDown}
      >
        <ListItemContent isOpen={isOpen}>{children}</ListItemContent>
      </li>
      <SubList isLast={isLast} isOpen={isOpen} section={section} />
    </OutsideAlerter>
  )
}

export default ListItem