export enum PageSections {
    Project = "project",
    WetLab = "wetLab",
    DryLab = "dryLab",
    HP = "hp",
    People = "people",
  }
  
  export type PageRoute = {
    route: string
    label: string
  }
  
  export type PageRoutesMapData = {
    [key in PageSections]: PageRoute[]
  }
  
  export const PageRoutesMap: PageRoutesMapData = {
    [PageSections.Project]: [
      { label: "Description", route: "description" },
      { label: "Design", route: "design" },
      { label: "Contribution", route: "contribution" },
      { label: "Implementation", route: "implementation" },
      { label: "Safety", route: "safety" },
    ],
    [PageSections.WetLab]: [
      { label: "Engineering", route: "engineering" },
      { label: "Proof of Concept", route: "proof-of-concept" },
      { label: "Experiments", route: "experiments" },
      { label: "Results", route: "results" },
      { label: "Part Collection", route: "part-collection" },
    ],
    [PageSections.DryLab]: [
      { label: "Hardware", route: "hardware" },
      { label: "Measurement", route: "measurement" },
      { label: "Model", route: "model" },
      { label: "Software", route: "software" },
    ],
    [PageSections.HP]: [
      { label: "Integrated Human Practices", route: "human-practices" },
      { label: "Communication", route: "communication" },
      { label: "Education", route: "education" },
      { label: "Sustainable Development", route: "sustainable" },
    ],
    [PageSections.People]: [
      { label: "Team", route: "team" },
      { label: "Attributions", route: "attributions" },
      { label: "Partnership", route: "partnership" },
    ],
  }
  