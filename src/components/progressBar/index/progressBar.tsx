import React from "react"
import "./progressBar.css"
//import progressBars from "../../static/svgs/progressBar"
//import useProgress from "./hooks/useProgress"

type Props = {
  isShown?: boolean
  footerHeight?: number
}

const ProgressBar = ({ isShown = true, footerHeight = 600 }: Props) => {
  //const triangles = useProgress(footerHeight)

  return (
     <div
      className={`progressBar ${isShown ? "active" : ""}`}
    >
    </div>
  )
}

export default ProgressBar
