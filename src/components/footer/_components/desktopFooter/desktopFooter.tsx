//import React from "react";
import "./style.css";
import {
  mockpractica,
  mockthermaks,
  mockgmc,
  mocklogo,
  mockWfacebook,
  realLogo
 } from "../../../../importStatic/mockData/images"

const DesktopFooter = () => (
  <div className="footerContainer">
    <div className="logoSocialContainer">
        <div className="logoContainer">
          <img className= "imageLogo" src={realLogo}></img>
        </div>
        <div className="socialContainer">
          <div className="socialContainer1">
            <a className="displayButtonONLY" href="https://www.facebook.com/VilniusiGEM?locale=lt_LT" target="_blank"><button className="imageSocialButton"><img className="imageSocial" src={mockWfacebook}></img></button></a>
            <a className="displayButtonONLY" href="https://www.facebook.com/VilniusiGEM?locale=lt_LT" target="_blank"><button className="imageSocialButton"><img className="imageSocial" src={mockWfacebook}></img></button></a>
            <a className="displayButtonONLY" href="https://www.facebook.com/VilniusiGEM?locale=lt_LT" target="_blank"><button className="imageSocialButton"><img className="imageSocial" src={mockWfacebook}></img></button></a>
          </div>
          <div className="socialContainer2">
          <a className="displayButtonONLY" href="https://www.facebook.com/VilniusiGEM?locale=lt_LT" target="_blank"><button className="imageSocialButton"><img className="imageSocial" src={mockWfacebook}></img></button></a>
          <a className="displayButtonONLY" href="https://www.facebook.com/VilniusiGEM?locale=lt_LT" target="_blank"><button className="imageSocialButton"><img className="imageSocial" src={mockWfacebook}></img></button></a>
          </div>  
        </div>
    </div>
    <div className="sponsorContainer">
      <div className="sponsorBig">
      <img className = "imageSponsorB"alt="Error404" src={mockgmc}></img>
      <img className = "imageThermofisher"alt="Error404" src={mockthermaks}></img>
      <img className = "imageSponsorB"alt="Error404" src={mockgmc}></img>
      </div>        
      <div className="sponsorSmall">
        <img className = "imageSponsorS"alt="Error404" src={mockpractica}></img>
        <img className = "imageSponsorS"alt="Error404" src={mockpractica}></img>
        <img className = "imageSponsorS"alt="Error404" src={mockpractica}></img>
        <img className = "imageSponsorS"alt="Error404" src={mockpractica}></img>
        <img className = "imageSponsorS"alt="Error404" src={mockpractica}></img>
      </div>
    </div>
  </div>
);
export default DesktopFooter
