import { CSSProperties, ReactNode } from "react"

export interface ITextProps {
    children: ReactNode
    //color?: TextColor
    //weight?: TextWeight
    //marginLeft?: TextSpace
    //marginRight?: TextSpace
    //paddingLeft?: TextSpace
    //paddingRight?: TextSpace
    style?: CSSProperties
  }
  
  