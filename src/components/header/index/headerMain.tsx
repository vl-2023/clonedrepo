import React from "react"
import { meskHeader } from "../../../importStatic/mockData/images"
import { MainHeaders, MainHeadersData } from "../headerTypes"
import "./headerMain.css"



const MainHeader = () => {
  return (
    <div
      className={`mainHeader`}
    >
      <img className="headerImg"src={meskHeader} alt="meskys"/>
      
    </div>
  )
}

export default MainHeader
