

export enum MainHeaders {
  Attributions,
  Description,
  Contribution,
  Engineering,
  Collaborations,
  HumanPractices,
  Implementation,
  PartCollection,
  Model,
  ProofOfConcept,
  Partnership,
  Communication,
  Education,
  Hardware,
  Measurement,
  Safety,
  Software,
  Sustainable,
  Design,
  Experiments,
  Results,
  Team,
}

export const MainHeadersData = [
  {
    pageName: "Attributions",
    url:'{}'
  },
  {
    pageName: "Description",
    url:'{}'
  },
  {
    pageName: "Contribution",
    url:'{}'
  },
  {
    pageName: "Engineering",
    url:'{}'
  },
  {
    pageName: "Collaborations",
    url:'{}'
  },
  {
    pageName: "Human practices",
    url:'{}'
  },
  {
    pageName: "Implementation",
    url:'{}'
  },
  {
    pageName: "Part Collection",
    url:'{}'
  },
  {
    pageName: "Model",
    url:'{}'
  },
  {
    pageName: "Proof of Concept",
    url:'{}'
  },
  {
    pageName: "Partnership",
    url:'{}'
  },
  {
    pageName: "Communication",
    url:'{}'
  },
  {
    pageName: "Education",
    url:'{}'
  },
  {
    pageName: "Hardware",
    url:'{}'
  },
  {
    pageName: "Measurement",
    url:'{}'
  },
  {
    pageName: "Safety",
    url:'{}'
  },
  {
    pageName: "Software",
    url:'{}'
  },
  {
    pageName: "Sustainability",
    url:'{}'
  },
  {
    pageName: "Design",
    url:'{}'
  },
  {
    pageName: "Experiments",
    url:'{}'
  },
  {
    pageName: "Results",
    url:'{}'
  },
  {
    pageName: "Team",
    url:'{}'
  },
]
