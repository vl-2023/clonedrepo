import React, { useRef } from "react"
import useOutsideAlerter from "../../customHooks/useOutsideAlerter"

type Props = {
  onClick: () => void
  children: React.ReactNode
}

export default function OutsideAlerter({ onClick, children }: Props) {
  const wrapperRef = useRef(null)
  useOutsideAlerter(wrapperRef, onClick)

  return <div ref={wrapperRef}>{children}</div>
}
