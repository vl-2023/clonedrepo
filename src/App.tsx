import { Route, Routes } from "react-router-dom"
import CollaborationsPage from "./pages/collaborations/collaborations"
import AttributionsPage from "./pages/attributions/attributions"
import CommunicationPage from "./pages/communication/communication"
import ContributionPage from "./pages/contribution/contribution"
import LandingPage from "./pages/landingPage/landingPage"
import DescriptionPage from "./pages/description/description"
import SoftwarePage from "./pages/software/software"

function App() {
  return (
    <Routes>
      <Route path="/vilnius-lithuania/" element={<LandingPage/>}/>
      <Route path="/vilnius-lithuania/attributions" element={<AttributionsPage/>}/>
      <Route path="/vilnius-lithuania/collaborations" element={<CollaborationsPage/>}/>
      <Route path="/vilnius-lithuania/communication" element={<CommunicationPage/>}/>
      <Route path="/vilnius-lithuania/contribution" element={<ContributionPage/>}/>
      <Route path="/vilnius-lithuania/description" element={<DescriptionPage/>}/>
      <Route path="/vilnius-lithuania/design" element=""/>
      <Route path="/vilnius-lithuania/education" element=""/>
      <Route path="/vilnius-lithuania/engineering" element=""/>
      <Route path="/vilnius-lithuania/experiments" element=""/>
      <Route path="/vilnius-lithuania/hardware" element=""/>
      <Route path="/vilnius-lithuania/human-practices" element=""/>
      <Route path="/vilnius-lithuania/implementation" element=""/>
      <Route path="/vilnius-lithuania/measurement" element=""/>
      <Route path="/vilnius-lithuania/model" element=""/>
      <Route path="/vilnius-lithuania/part-collection" element=""/>
      <Route path="/vilnius-lithuania/partrnership" element=""/>
      <Route path="/vilnius-lithuania/proof-of-concept" element=""/>
      <Route path="/vilnius-lithuania/results" element=""/>
      <Route path="/vilnius-lithuania/safety" element=""/>
      <Route path="/vilnius-lithuania/software" element={<SoftwarePage/>}/>
      <Route path="/vilnius-lithuania/sustainable" element=""/>
      <Route path="/vilnius-lithuania/team" element=""/>
    </Routes>
  )
}

export default App
